# openshift-on-vmware

Installing OpenShift 4.x on VMWare, with static IP addresses

## Files

### install-config.yaml

Reference install configuration. Needs to be modified on a per cluser basis. Advisable to change the file in this directory, then copy to the install directory, as it is automatically deleted when the ignition files are made. 


### config\_bootstrap.txt

Reference file containing environment variables. Needs to be modified on a per cluster basis. Used by the config\_bootstrap.sh script. 


### config\_bootstrap.sh 

Script when run creates individual ignition files for the bootstrap, masters and workers based on the paramters in the config\_bootstrap.txt  file.
It also creates the BIND-formatted dns configuraiton file, and the dhcp configuration file.

## Using the files

Start off in the directory with these files in.

First edit the `install-config.yaml` file to reflect settings for the cluster.

Run this command to create the initial ignition files (here in the ~/ocp4 directory)

    rm -rf ~/ocp4
    mkdir ~/ocp4
    cp install-config.yaml ~/ocp4/
    openshift-install create ignition-configs --dir ~/ocp4

To run `config\_bootstrap.sh`, optionally set and export these environment variables (if the defaults aren't appropriate):

- NGINX\_DIRECTORY is the directory containing the created igntion files (defaults to the current directory).

- ETH\_DEV is the name of the main Ethernet interface on this host (defaults to `ens192`, for RHEL servers).

Optionally change directory to the one used in the `openshift-install` command above
(particularly if you haven't set the NGINX\_DIRECTORY variable), then run the script

    config_bootstrap.sh
